from django.shortcuts import render

# Create your views here.

def homePage(request):
	return render(request, 'home.html')

def lelangSekarang(request):
    if request.user.is_authenticated:
        return render(request, 'page_lelang.html')
    else:
        return render(request, 'loginpage.html')

def loginPage(request):
    return render(request, 'loginpage.html')
def ngebid(request):
	return render(request, 'page_bid.html')
def Kirim(request):
    if request.user.is_authenticated:
        return render(request,'Pengiriman.html')

    else:
        return render(request, 'loginpage.html')
    
