from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from . import views



urlpatterns =  [
    path('', views.homePage, name='home'),
    path('home2/',views.homePage),
    path('lelang/', views.lelangSekarang),
    path('loginpage/', views.loginPage),
    path('bid/', views.ngebid),
    path('kirim/',views.Kirim),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
]